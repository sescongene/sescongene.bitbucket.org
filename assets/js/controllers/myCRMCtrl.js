
//testuser2@loanmarket.co.nz / test302 

app.controller('ModalInstanceCtrl', ["$scope", "$rootScope", "$uibModalInstance", "$http", "SweetAlert", "$cookies", "$location", function ($scope, $rootScope, $uibModalInstance, $http, SweetAlert, $cookies, $location) {
    $scope.save_client = function () {
        console.log($scope.newclient)
        try {
            $scope.newclient.DateOfBirth = $scope.newclient.DateOfBirthx.toISOString();
        } catch (err) {
            console.log("No Date");
        }
        $scope.newclient.DOB = $scope.newclient.DateOfBirth;
        $scope.newclient.FamilyId = $rootScope.fam_id;
        $scope.newclient.Phone = [
            {
                "Type": "Home",
                "Number": $scope.home_number
            },
            {
                "Type": "Work",
                "Number": $scope.work_number
            },
            {
                "Type": "Mobile",
                "Number": $scope.mobile_number
            }
        ]

        var req = {
            method: 'POST',
            url: "https://testapi.nzfsg.co.nz/contacts/ContactSet",
            headers: {
                "Authorization": "Bearer " + $cookies.get('auth'),
            },
            data: [$scope.newclient]
        }
        $http(req).then(function successCallback(response) {
            SweetAlert.swal({
                title: "Client Saved",
                text: "New Client has been created",
                type: "success",
            },
                function () {
                    console.log("test");
                });
        }, function errorCallback(response) {
            console.log(response);
            $uibModalInstance.dismiss('cancel');
            $uibModalInstance.close();
        });
        get_clients();
    }

    function get_clients() {
        $location.path("/app/contacts/view/" + $rootScope.fam_id);
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

app.controller('addrelCtrl', ["$scope", "$state", "$http", "$location", "$rootScope", '$cookies', "$stateParams", 'SweetAlert', "$uibModal", function ($scope, $state, $http, $location, $rootScope, $cookies, $stateParams, SweetAlert, $uibModal) {
    $scope.open = function (size) {
        var modalInstance = $uibModal.open({
            templateUrl: 'add_rel.html',
            controller: 'ModalInstanceCtrl',
            size: 'sm',

        });
        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            
        }, function () {
            
        });
    };
}]);

app.controller('addClientCtrl', ["$scope", "$state", "$http", "$location", "$rootScope", '$cookies', "$stateParams", 'SweetAlert', "$uibModal", function ($scope, $state, $http, $location, $rootScope, $cookies, $stateParams, SweetAlert, $uibModal) {
    $scope.open = function (size) {
        var modalInstance = $uibModal.open({
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            size: size,

        });
        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            console.log("1");
        }, function () {
            console.log("2");
        });
    };


}]);


app.controller('contactViewCtrl', ["$scope", "$state", "$http", "$location", "$rootScope", '$cookies', "$stateParams", function ($scope, $state, $http, $location, $rootScope, $cookies, $stateParams) {
    $scope.params = $stateParams;
    if ($scope.params.id === null) {

        $location.path("/app/contacts");
    } else {
        $rootScope.fam_id = $scope.params.id;
    }

    if (typeof $rootScope.contacts === "undefined") {
        console.log("testx");
        fetch_contacts();
    } else {
        $rootScope.contacts.forEach(v => {
            if (v.FamilyID == $scope.params.id)
                return $scope.family = v
        })
    }

    var req = {
        method: 'GET',
        url: 'https://testapi.nzfsg.co.nz/contacts/TaggedList?familyID=' + $scope.params.id,
        headers: {
            "Authorization": "Bearer " + $cookies.get('auth'),
        }
    }
    $http(req).then(function successCallback(response) {
        $scope.tagged_list = response.data;
    }, function errorCallback(response) {
        console.log(response);
    });


    function get_details() {
        $http({
            method: 'GET',
            url: 'https://testapi.nzfsg.co.nz/contacts/ContactFamilyInfoGet?familyId=' + $scope.params.id,
            headers: {
                "Authorization": "Bearer " + $cookies.get('auth'),
            }
        }).then(function successCallback(response) {
            $scope.family_info = response.data;
        }, function errorCallback(response) {
            console.log(response);
        });
    }

    function get_clients() {
        $http({
            method: 'GET',
            url: 'https://testapi.nzfsg.co.nz/contacts/ClientInformGet?familyId=' + $scope.params.id,
            headers: {
                "Authorization": "Bearer " + $cookies.get('auth'),
            }
        }).then(function successCallback(response) {
            $scope.clients = response.data;
        }, function errorCallback(response) {
            console.log(response);
        });
    }

    function fetch_contacts() {
        $http({
            type: 'GET',
            url: 'https://testapi.nzfsg.co.nz/contacts/FamilyListGet?startWith=*&byPassFilter=true',
            headers: {
                "Authorization": "Bearer " + $cookies.get('auth'),
            }
        }).then(function successCallback(response) {
            response.data.FamilyList.forEach(v => {
                if (v.FamilyID == $scope.params.id)
                    return $scope.family = v
            })

        }, function errorCallback(response) {
            console.log(response);
        });
    }

    function get_loans() {
        $http({
            type: 'GET',
            url: 'https://testapi.nzfsg.co.nz/contacts/LoanListGet?familyID=' + $scope.params.id,
            headers: {
                "Authorization": "Bearer " + $cookies.get('auth'),
            }
        }).then(function successCallback(response) {
            $scope.loans = response.data;
            if ($scope.loans.length == 0) {
                $scope.no_loans = true;
            }
            console.log($scope.loans);

        }, function errorCallback(response) {
            console.log(response);
        });

    }

    get_loans();
    get_details();
    get_clients();
}]);


app.controller('ContactListCtrl', ["$scope", "$rootScope", "$cookies", "$http", function ($scope, $rootScope, $cookies, $http) {
    console.log("tesst");
    $http({
        type: 'POST',
        url: "https://testapi.nzfsg.co.nz/login",
        data: {
            "username": "testuser1@loanmarket.co.nz",
            "password": "test345",
        },
        method: "POST",
    }).then(function successCallback(response) {
        $cookies.put('auth', response.data);
        fetch_contacts();
    },
        function errorCallback(response) {
            console.log(response);
        });

    function fetch_contacts() {
        $http({
            type: 'GET',
            url: 'https://testapi.nzfsg.co.nz/contacts/FamilyListGet?startWith=*&byPassFilter=true',
            headers: {
                "Authorization": "Bearer " + $cookies.get('auth'),
            }
        }).then(function successCallback(response) {
            console.log(response.data);
            $scope.contacts = response.data.FamilyList;
            $rootScope.contacts = response.data.FamilyList;
            checkall();
        }, function errorCallback(response) {
            console.log(response);
        });
    }

    function checkall() {
        console.log("x");
        var parent_chkbx = $('#checkall');
        parent_chkbx.click(function () {
            var checkbox = $('.contact-table-body-x input[type=checkbox]');
            if (parent_chkbx.prop('checked') === true) {
                checkbox.prop('checked', function () {
                    $('.contact-table-body-x tr').addClass('active-x');
                    return true;
                });
            } else {
                checkbox.prop('checked', false);
                $('.contact-table-body-x tr').removeClass('active-x');

            }
        });
        $('.select-contact').click(function () {
            var x = $(this);
            var y = x.prop('checked');
            if (y === true) {
                x.parents("tr").addClass('active-x');
            } else {
                x.parents("tr").removeClass('active-x');
            }
        });
    }
}]);


app.controller('crmLoginCtrl', ["$scope", "$rootScope", "$http", "SweetAlert", "$cookies", "$location", function ($scope, $rootScope, $http, SweetAlert, $cookies, $location) {
    
}]);